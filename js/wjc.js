Vue.component('wj-code', {
    props: {
        beforePlaceholder: {
            type: String,
            default: '加载中...'
        },
        placeholder: {
            type: String,
            default: '向右拖动滑块'
        },
        showRefresh: {
            type: Boolean,
            default: true
        },
        imageWidth: {
            type: [String, Number],
            default: 220
        },
        imageHeight: {
            type: [String, Number],
            default: 130
        },
        puzzleSize: { // 拼图大小
            type: [String, Number],
            default: 30
        },
        images: {
            type: Array,
            default: () => [`https://picsum.photos/220/130`]
        },
        errorTime: {
            type: [String, Number],
            default: 1000
        },
        region: { // 波动范围
            type: [String, Number],
            default: 5
        }
    },
    data () {
        return {
            showPlaceholder: true, // 开始验证时隐藏
            isMove: false, // 滑块是否在滑动
            slideLeft: 0, // 滑块距左边的距离
            imageSrc: '', // 最后显示图片的地址
            imageLoaded: false, // 图片是否加载成功
            shouldMove: 0, // 应该移动距离
            isSuccess: 0, // 是否验证成功(0 未验证 1 成功 2 失败)
            showImageContainer: false
        };
    },
    computed: {
        containerClass () {
            return {
                'verfication-code': true,
                success: this.isSuccess === 1,
                error: this.isSuccess === -1 && this.slideLeft > 0
            };
        },
        slideStyle () { // 滑块样式
            return {
                left: this.slideLeft + 'px'
            };
        },
        outerStyle () {
            return {
                width: this.slideLeft + 'px',
                visibility: this.slideLeft === 0 ? 'hidden' : 'visible'
            };
        },
        imagesCount () {
            return this.images.length;
        },
        puzzleWidth () {
            return parseInt(this.puzzleSize);
        }
    },
    methods: {
        check () { // 检验是否验证成功
            let region = parseInt(this.region);
            if (this.slideLeft <= this.shouldMove + region && this.slideLeft >= this.shouldMove - region) {
                this.isSuccess = 1;
                this.$emit('success');
            } else {
                this.isSuccess = -1;
                this.$emit('error');
            }
        },
        startMove (event) {
            if (!this.imageLoaded || this.isSuccess === 1) { // 图片加载完成才可以操作
                return;
            }
            let startX = event.clientX;
            this.showPlaceholder = false;
            this.isMove = true;
            this.showImageContainer = true;
            // 初始鼠标距左边的距离

            let that = this;

            function mouseMove (e) { // 验证成功时接触绑定
                // 可以移动的最大长度
                let maxLeft = that.$refs.code.clientWidth - that.$refs.slide.clientWidth;
                // 滑动过程鼠标距左边的距离
                let mouseX = e.clientX;

                that.slideLeft = Math.min(Math.max(0, mouseX - startX), maxLeft);
            }
            // 动作监听和移除
            Event.addEvent(document, 'mousemove', mouseMove);
            Event.addEvent(document, 'mouseup', function mouseUp () {
                that.check();
                that.isMove = false;
                Event.removeEvent(document, 'mousemove', mouseMove);
                Event.removeEvent(document, 'mouseup', mouseUp);
            });
        },
        refresh () {
            this.imageLoaded = false;
            this.clearImage();
            this.createImage();
        },
        getImageCanvas () {
            return this.$refs.imageCanvas;
        },
        getPuzzleCanvas () {
            return this.$refs.puzzleCanvas;
        },
        createImage () {
            let imageCanvas = this.getImageCanvas();
            let puzzleCanvas = this.getPuzzleCanvas();

            let imageCtx = imageCanvas.getContext('2d');
            let puzzleCtx = puzzleCanvas.getContext('2d');

            let width = this.puzzleWidth;
            // 拼图实际长度
            let realWidth = width * (1 + (2 + Math.sqrt(2)) / 8) + 4;
            // 随机生成拼图位置
            let x = this.shouldMove = getRandom(realWidth, (this.imageWidth - realWidth));
            let y = getRandom(width, (this.imageHeight - realWidth));

            let img = new Image();
            img.crossOrigin = 'Anonymous';
            img.src = this.images[getRandom(0, this.imagesCount - 1)];
            img.onload = () => {
                this.drawImage(img, imageCtx, x, y, 'fill');
                this.drawImage(img, puzzleCtx, x, y, 'clip');
                let imageData = puzzleCtx.getImageData(x, y - width * ((2 + Math.sqrt(2)) / 8),
                    realWidth,
                    realWidth);
                puzzleCanvas.width = realWidth;
                puzzleCtx.putImageData(imageData, 0, y - width * ((2 + Math.sqrt(2)) / 8));
                this.imageLoaded = true;
            };
            img.onerror = () => {
                this.createImage();
            };
        },
        drawImage (img, ctx, x = 0, y = 0, type = 'fill') {
            if (!img || !ctx) {
                return;
            }
            // 正方形
            let width = this.puzzleWidth;

            ctx.translate(x, y);
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.lineTo((width - (Math.sqrt(2) / 4) * width) / 2, 0);
            ctx.arc(width / 2, -width * (Math.sqrt(2) / 8), width / 4, Math.PI * 3 / 4, Math.PI / 4);
            ctx.lineTo(width, 0);
            ctx.lineTo(width, (width - (Math.sqrt(2) / 4) * width) / 2);
            ctx.arc(width * (1 + Math.sqrt(2) / 8), width / 2, width / 4, -Math.PI * 3 / 4, Math.PI * 3 /
                4);
            ctx.lineTo(width, width);
            ctx.lineTo(0, width);
            ctx.arc(width * (Math.sqrt(2) / 8), width / 2, width / 4, Math.PI * 3 / 4, -Math.PI * 3 / 4,
                true);
            ctx.closePath();


            ctx.lineWidth = 2;

            if (type === 'fill') {
                ctx.strokeStyle = '#ffffff';
                ctx.fillStyle = '#ffffff50';
                ctx.globalCompositeOperation = 'dustination-in';
            } else if (type === 'clip') {
                ctx.strokeStyle = '#aaaaaa';
            }
            ctx.stroke();
            ctx[type]();
            ctx.globalCompositeOperation = 'destination-over';

            ctx.drawImage(img, -x, -y, this.imageWidth, this.imageHeight);
        },
        clearImage () {
            let imageCanvas = this.getImageCanvas();
            let puzzleCanvas = this.getPuzzleCanvas();
            imageCanvas.setAttribute('width', this.imageWidth);
            imageCanvas.setAttribute('height', this.imageHeight);
            puzzleCanvas.setAttribute('width', this.imageWidth);
            puzzleCanvas.setAttribute('height', this.imageHeight);
        }
    },
    mounted () {
        this.createImage();
    },
    watch: {
        isSuccess (val) {
            if (val === -1) {
                setTimeout(() => {
                    this.isSuccess = 0;
                }, parseInt(this.errorTime));
            } else if (this.isSuccess === 0) {
                this.slideLeft = 0;
                this.showPlaceholder = true;
            }
        }
    },
    template: `
        <div :class="containerClass" ref="code">
            <!-- 最里面显示文字 -->
            <div class="inner">
                <!-- 文字 -->
                <p v-html="imageLoaded ? placeholder : beforePlaceholder" v-show="showPlaceholder"></p>
            </div>
            <!-- 滑块 -->
            <div :class="['slide', imageLoaded ? '' : 'unready', isMove ? 'moving' : '']" :style="slideStyle"
                @mousedown="startMove($event)" ref="slide">
                <!-- 图标 -->
                <span
                    :class="{'icon': true, 'icon-checkmark': isSuccess === 1, 'icon-arrow-right': isSuccess === 0, 'icon-err': isSuccess === -1}"></span>
            </div>
            <!-- 滑块划过的地方 -->
            <div class="outer" :style="outerStyle"></div>
            <!-- 显示图片 -->
            <div :class="['image-container', showImageContainer && isSuccess !== 1 ? 'show' : '']">
                <!-- 包裹图标 -->
                <div :style="{width: imageWidth + 'px', height: imageHeight + 'px'}">
                    <span class="icon-spinner" v-show="!imageLoaded"></span>
                    <canvas :class="['puzzle-canvas', isMove ? 'moving' : '']" :width="imageWidth" :height="imageHeight"
                        :style="{left: slideLeft + 'px'}" ref="puzzleCanvas"></canvas>
                    <canvas class="image-canvas" :width="imageWidth" :height="imageHeight" :title="placeholder"
                        ref="imageCanvas"></canvas>
                    <!-- 刷新按钮，默认显示 -->
                    <span class="icon icon-refresh" v-if="showRefresh" title="刷新" @click="refresh"></span>
                    <!-- 可额外添加按钮 -->
                    <slot name="icon"></slot>
                </div>
            </div>
        </div>`
});

const Event = {
    _events: {},
    addEvent (target, type, fn, ...args) {
        this._events[fn] = (e) => {
            fn.apply(this, [e, ...args]);
        };
        if (window.addEventListener) {
            target.addEventListener(type, this._events[fn], false);
        } else if (window.attachEvent) {
            target.attachEvent('on' + type, this._events[fn]);
        } else {
            target['on' + type] = this._events[fn];
        }
    },
    removeEvent (target, type, fn) {
        for (let key in this._events) {
            if (key === fn.toString()) {
                let item = this._events[key];
                delete this._events[key];

                if (window.removeEventListener) {
                    target.removeEventListener(type, item);
                } else if (window.detachEvent) {
                    target.detachEvent('on' + type, item);
                } else {
                    target['on' + type] = null;
                }
            }
            break;
        }
    }
};

const getRandom = (...args) => {
    let min = args[0];
    let max = args[1];

    switch (args.length) {
        case 1:
            return parseInt(Math.random() * min + 1, 10);
        case 2:
            return parseInt(Math.random() * (max - min + 1) + min, 10);
        default:
            return 0;
    }
};
